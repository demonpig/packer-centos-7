# packer-centos-7
Minimalist CentOS build using Packer.

| Provider    | Supported  |
|-------------|------------|
| Virtualbox  | Yes        |
| LibVirt     | No         |

## Installation
* Install packer, vagrant, virtualbox

## Build

Vagrant:
```
packer build vagrant.json
```

## References
* [Jeff Geerling](https://github.com/geerlingguy/packer-centos-7)
* [CentOS Cloud SIG](https://github.com/CentOS/sig-cloud-instance-build)